import {Injectable} from '@angular/core';
import {Http, Headers, RequestOptions, Response} from '@angular/http';
import {Subject} from 'rxjs/Subject';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class DataService {

  constructor(
    private http: Http) {
  }

  getTest() {
    return this.http.get('/services/test').map((response: Response) => response.json());
  }


  private headers() {
    let headers = new Headers({'Content-Type': 'application/json'});
    return new RequestOptions({headers: headers});
  }

  private multiFilesHeaders() {
    let mHeaders = new Headers({
      'Content-Type': 'multipart/form-data',
      'Accept': 'application/json'
    });
    return new RequestOptions({headers: mHeaders});
  }
}
