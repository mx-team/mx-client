import { Component } from '@angular/core';
import {DataService} from './_services/index';
import {Form,FormElement,ElementType} from './_models/index';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  form : Form;

    constructor(private dataService: DataService) {
		this.form = {name:'hz',elements:[{name:'elem1',field:'elem1',type:ElementType.INPUT},{name:'elem2',field:'elem2',type:ElementType.INPUT_NUMBER},{name:'elem3',field:'elem3',type:ElementType.TEXT_AREA}]};
    }

    testClick(){
      this.dataService.getTest().subscribe(data => {
        console.log(data);
      });
    }
}
