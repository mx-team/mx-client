import { BrowserModule } from '@angular/platform-browser';
import { FormsModule }   from '@angular/forms';
import { HttpModule } from '@angular/http';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';

import { DataService } from './_services/index';

import { MxFormComponent } from './_components/index';
// import { FormComponent } from 'ngx-mx-library';


@NgModule({
  declarations: [
    AppComponent,
    MxFormComponent
    // FormComponent
  ],
  imports: [
    BrowserModule,FormsModule,HttpModule
  ],
  providers: [DataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
