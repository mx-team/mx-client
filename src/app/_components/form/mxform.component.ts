import {Component,Input, OnInit,ElementRef,ViewChild} from '@angular/core';
import {Form,FormElement,ElementType} from '../../_models/index';

@Component({
  selector: 'mx-form',
  templateUrl: 'mxform.component.html'
})
export class MxFormComponent implements OnInit {
  @Input() form: Form;
  @ViewChild('f') f: ElementRef;  
  model: any = {};
  loading: boolean = false;
  elementType = ElementType;

  constructor() {

  }
  
  ngOnInit() {
    console.log(this.form,ElementType.INPUT_NUMBER);
  }
  
  isTypeValid(fieldType:ElementType,formType:ElementType){
  	return fieldType == formType;
  }
  
  isValid(element:FormElement){
  	return false;
  }
  
  submit(){
  	console.log(this.model);
  }
}