export enum ElementType {
	INPUT,
	INPUT_NUMBER,
	TEXT_AREA
}

export interface FormElement {
  name: string;
  field : string;
  type: ElementType;
  value?: any;
}