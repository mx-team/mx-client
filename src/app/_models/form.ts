import {FormElement} from './form-element';

export interface Form {
  name: string;
  elements : FormElement[];
}