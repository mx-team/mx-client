const PROXY_CONFIG = [
    {
        context: [
            "/api",
            "/services",
            "/login",
            "/logout",
            "/images"
        ],
        target: "http://localhost:3003",
        secure: false
    }
]

module.exports = PROXY_CONFIG;